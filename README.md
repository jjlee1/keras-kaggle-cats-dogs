This goes along with this tutorial:

https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html

I'm mostly just publishing this for the benefit of a few people I know who are
playing with deep learning, but maybe somebody else finds it useful.

It's a slightly cleaned up version of the scripts used in the tutorial, which
also shows how to evaluate models.  I think I also fixed some stuff that wasn't
working for me.

* If you're using your own GPU, I found this useful to get cuDNN and CUDA
  installed (AWS has an AMI with everything installed, similar with other cloud
  providers):
  https://askubuntu.com/questions/767269/how-can-i-install-cudnn-on-ubuntu-16-04

* Ensure the following python projects are installed (they all work in a Python
  3 virtualenv):

    keras
    tensorflow
    h5py
    opencv-python

  Possibly you might need to install these too, I don't remember:

    pillow
    scipy

* Download the kaggle dataset referenced in the tutorial (you'll need a kaggle
  login)
* Unpack the data
* cd to the directory that *contains* directory `train`
* Run `make-cats-dogs-sets` to create a directory named `data`
* Run `train1.py --weights-file my-weights.h5 --train`
* Run `train1.py --weights-file my-weights.h5 --evaluate`
* `mkdir -p my-weights-dir`
* Run `train2.py --weights-dir my-weights-dir --save-vgg16-features`
* Run `train2.py --weights-dir my-weights-dir --train`
* Run `train2.py --weights-dir my-weights-dir --evaluate`

I didn't clean up `train3.py`, but this should show you what I did...

Edit the directory in `evaluate()` to run evaluation against either cats or
dogs.

On my GPU (Geforce GTX 1060, using cuDNN), training for train1.py takes about
20 seconds per epoch (x 50 epochs, so about 15 minutes).  On a CPU it's maybe
10 or 20 times slower.

Saving the VGG16 features from train2.py takes maybe a minute or two, and
training for train2.py takes maybe 5 minutes.  Evaluation is much faster.

For me, on a GPU (see note below), train1.py --train produces networks that
vary in their results (on the validation set) quite a lot:

Run #1:

35 / 400 dogs are cats

250 / 400 cats are dogs (oops)

Run #2:

97 / 400 dogs are cats

139 / 400 cats are dogs


I think there are multiple sources of non-determinism.


The trained model created by train2.py thinks:

17 / 400 dogs are cats

50 / 400 cats are dogs


I consistently got different results on one of these scripts on a CPU: both the
loss and the accuracy went bad, presumably learning rate too high?  But I don't
know why my CPU would see that behaviour and not the GPU, again haven't
investigated (yet: I'm curious why it happens).
