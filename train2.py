'''This script goes along the blog post
"Building powerful image classification models using very little data"
from blog.keras.io.
It uses data that can be downloaded at:
https://www.kaggle.com/c/dogs-vs-cats/data
In our setup, we:
- created a data/ folder
- created train/ and validation/ subfolders inside data/
- created cats/ and dogs/ subfolders inside train/ and validation/
- put the cat pictures index 0-999 in data/train/cats
- put the cat pictures index 1000-1400 in data/validation/cats
- put the dogs pictures index 12500-13499 in data/train/dogs
- put the dog pictures index 13500-13900 in data/validation/dogs
So that we have 1000 training examples for each class, and 400 validation examples for each class.
In summary, this is our directory structure:
```
data/
    train/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
    validation/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
```
'''

import argparse
import glob
import os
import sys

import cv2
import numpy as np


# dimensions of our images.
img_width, img_height = 150, 150
image_shape = (img_width, img_height, 3)

train_data_dir = 'data/train'
validation_data_dir = 'data/validation'
nb_train_samples = 2000
nb_validation_samples = 800
epochs = 50
batch_size = 16


def evaluate_and_save_vgg16_features(model, dir_path):
    from keras.preprocessing.image import ImageDataGenerator
    datagen = ImageDataGenerator(rescale=1. / 255)

    generator = datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)
    bottleneck_features_train = model.predict_generator(
        generator, nb_train_samples // batch_size)
    train_path = os.path.join(dir_path, 'bottleneck_features_train.npy')
    with open(train_path, 'wb') as fh:
        np.save(fh, bottleneck_features_train)

    generator = datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)
    bottleneck_features_validation = model.predict_generator(
        generator, nb_validation_samples // batch_size)
    validation_path = os.path.join(
        dir_path, 'bottleneck_features_validation.npy')
    with open(validation_path, 'wb') as fh:
        np.save(fh, bottleneck_features_validation)


def make_top_layer_model(input_shape):
    from keras.layers import Dropout, Flatten, Dense
    from keras.models import Sequential
    model = Sequential()
    model.add(Flatten(input_shape=input_shape))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(optimizer='rmsprop',
                  loss='binary_crossentropy', metrics=['accuracy'])
    return model


def train_top_layer_model(model, dir_path):
    train_path = os.path.join(dir_path, 'bottleneck_features_train.npy')
    with open(train_path, 'rb') as fh:
        train_data = np.load(fh)
    train_labels = np.array(
        [0] * (nb_train_samples // 2) +
        [1] * (nb_train_samples // 2))

    validation_path = os.path.join(
        dir_path, 'bottleneck_features_validation.npy')
    with open(validation_path, 'rb') as fh:
        validation_data = np.load(fh)
    validation_labels = np.array(
        [0] * (nb_validation_samples // 2) +
        [1] * (nb_validation_samples // 2))

    model.fit(train_data, train_labels,
              epochs=epochs,
              batch_size=batch_size,
              validation_data=(validation_data, validation_labels))


def compose_models(*models):
    from keras.models import Sequential
    model = Sequential()
    for component_model in models:
        model.add(component_model)
    return model


def evaluate(model):
    for path in glob.glob('data/validation/dogs/*.jpg'):
        x = cv2.imread(path)
        x = cv2.resize(x, (img_width, img_height))
        x = np.expand_dims(x, axis=0)
        classes = model.predict_classes(x)
        category = classes[0][0]
        print("path %s is a " % (path, ), end='')
        print(['cat', 'dog'][category])


def parse_arguments(prog, args):
    parser = argparse.ArgumentParser(prog=prog)
    parser.add_argument(
        '--save-vgg16-features', action='store_true', default=False)
    parser.add_argument('--train', action='store_true', default=False)
    parser.add_argument('--evaluate', action='store_true', default=False)
    parser.add_argument('--weights-dir',
                        help='.h5 path in which to save and load weights')
    return parser.parse_args(args)


def keras_main(arguments):
    from keras import applications

    top_model_weights_path = os.path.join(
        arguments.weights_dir, 'bottleneck_fc_model.h5')

    # this will download the trained (on ImageNet) VGG16 network from the
    # keras website if it's not present locally
    base_model = applications.VGG16(
        include_top=False, weights='imagenet', input_shape=image_shape)
    if arguments.train or arguments.evaluate:
        top_model = make_top_layer_model(base_model.output_shape[1:])

    if arguments.save_vgg16_features:
        evaluate_and_save_vgg16_features(base_model, arguments.weights_dir)

    if arguments.train:
        train_top_layer_model(top_model, arguments.weights_dir)
        top_model.save_weights(top_model_weights_path)

    if arguments.evaluate:
        top_model.load_weights(top_model_weights_path)
        model = compose_models(base_model, top_model)
        evaluate(model)


def main(prog, args):
    import keras.backend
    arguments = parse_arguments(prog, args)
    with keras.backend.get_session():
        keras_main(arguments)


if __name__ == '__main__':
    sys.exit(main(sys.argv[0], sys.argv[1:]))
