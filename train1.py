'''This script goes along the blog post
"Building powerful image classification models using very little data"
from blog.keras.io.
It uses data that can be downloaded at:
https://www.kaggle.com/c/dogs-vs-cats/data
In our setup, we:
- created a data/ folder
- created train/ and validation/ subfolders inside data/
- created cats/ and dogs/ subfolders inside train/ and validation/
- put the cat pictures index 0-999 in data/train/cats
- put the cat pictures index 1000-1400 in data/validation/cats
- put the dogs pictures index 12500-13499 in data/train/dogs
- put the dog pictures index 13500-13900 in data/validation/dogs
So that we have 1000 training examples for each class, and 400 validation examples for each class.
In summary, this is our directory structure:
```
data/
    train/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
    validation/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
```
'''

import argparse
import glob
import sys

import cv2
import numpy as np


# dimensions of our images.
img_width, img_height = 150, 150

train_data_dir = 'data/train'
validation_data_dir = 'data/validation'
nb_train_samples = 2000
nb_validation_samples = 800
epochs = 50
batch_size = 16


def make_model():
    # Keras imports are slow because of CUDA setup so only import where needed
    from keras.layers import Activation, Dropout, Flatten, Dense
    from keras.layers import Conv2D, MaxPooling2D
    from keras.models import Sequential
    import keras.backend

    if keras.backend.image_data_format() == 'channels_first':
        input_shape = (3, img_width, img_height)
    else:
        input_shape = (img_width, img_height, 3)

    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    model.compile(loss='binary_crossentropy',
                optimizer='rmsprop',
                metrics=['accuracy'])

    return model


def train(model):
    from keras.preprocessing.image import ImageDataGenerator

    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='binary')

    model.fit_generator(
        train_generator,
        steps_per_epoch=nb_train_samples // batch_size,
        epochs=epochs,
        validation_data=validation_generator,
        validation_steps=nb_validation_samples // batch_size)


def evaluate(model):
    for path in glob.glob('data/validation/cats/*.jpg'):
        x = cv2.imread(path)
        x = cv2.resize(x, (img_width, img_height))
        x = np.expand_dims(x, axis=0)
        classes = model.predict_classes(x)
        score = model.predict(x)[0][0]
        category = classes[0][0]
        print("path %s is a " % (path, ), end='')
        print(['cat', 'dog'][category], 'score', score)


def parse_arguments(prog, args):
    parser = argparse.ArgumentParser(prog=prog)
    parser.add_argument('--train', action='store_true', default=False)
    parser.add_argument('--evaluate', action='store_true', default=False)
    parser.add_argument('--weights-file',
                        help='.h5 path in which to save and load weights')
    return parser.parse_args(args)


def keras_main(arguments):
    if arguments.train or arguments.evaluate:
        model = make_model()
    if arguments.train:
        train(model)
        model.save_weights(arguments.weights_file)
    if arguments.evaluate:
        model.load_weights(arguments.weights_file)
        evaluate(model)


def main(prog, args):
    import keras.backend
    arguments = parse_arguments(prog, args)
    with keras.backend.get_session():
        keras_main(arguments)


if __name__ == '__main__':
    sys.exit(main(sys.argv[0], sys.argv[1:]))
